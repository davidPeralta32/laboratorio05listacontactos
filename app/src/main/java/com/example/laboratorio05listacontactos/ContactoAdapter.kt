package com.example.laboratorio05listacontactos

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.laboratorio05listacontactos.Model.Contacto
import com.example.laboratorio05listacontactos.databinding.ActivityContactoBinding
import com.example.laboratorio05listacontactos.databinding.ItemContactoBinding

class ContactoAdapter(var Contactos :List<Contacto> = listOf()): RecyclerView.Adapter<ContactoAdapter.ContactoAdapterViewHolder>() {



    inner class ContactoAdapterViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){

        private val binding: ItemContactoBinding = ItemContactoBinding.bind(itemView)

        fun bind(contacto:Contacto) = with(binding){
            binding.tvNombre.text = contacto.nombre
            binding.tvCargo.text = contacto.cargo
            binding.tvCorreo.text = contacto.correo
            binding.tvLetraContacto.text = contacto.nombre.first().toString()
        }
    }

    fun updateListContacto(contactos:List<Contacto>){
        this.Contactos = contactos
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactoAdapterViewHolder {
        val view : View = LayoutInflater.from(parent.context).inflate(R.layout.item_contacto,parent,false)
        return ContactoAdapterViewHolder(view)
    }

    override fun onBindViewHolder(holder: ContactoAdapterViewHolder, position: Int) {
        val contacto = Contactos[position]
        holder.bind(contacto)
    }

    override fun getItemCount(): Int {
        return Contactos.size
    }
}