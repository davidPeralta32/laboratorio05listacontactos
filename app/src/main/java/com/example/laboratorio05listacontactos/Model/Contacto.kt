package com.example.laboratorio05listacontactos.Model

import java.io.Serializable

data class Contacto (val nombre:String, val cargo:String, val correo :String) :Serializable {

}