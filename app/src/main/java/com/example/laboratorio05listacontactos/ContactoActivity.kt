package com.example.laboratorio05listacontactos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.laboratorio05listacontactos.Model.Contacto
import com.example.laboratorio05listacontactos.databinding.ActivityContactoBinding

class ContactoActivity : AppCompatActivity() {

    private lateinit var binding : ActivityContactoBinding
    private lateinit var adaptador : ContactoAdapter
    private  var listContacto:List<Contacto> = listOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityContactoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        configurarAdaptador()

        cargarContactos()


    }

    private fun cargarContactos(){
        listContacto = listOf(
            Contacto("David Peralta Arenal","Android Developer","davi32peralta@gmail.com"),
            Contacto("Daniel Sanchez Peralta","Android Developer","dani_sam@gmail.com"),
            Contacto("Iris Garcia Peralta","Contador","iria18Garcia@gmail.com"),
            Contacto("Alan Garcia Peralta","Mantenimiento","alan_bna@gmail.com"),
            Contacto("Francisco Sanchez Peralta","Web Developer","francisco_Sam@gmail.com"),
        )
        adaptador.updateListContacto(listContacto)
    }

    private fun configurarAdaptador(){
        adaptador = ContactoAdapter()
        binding.rvContactos.adapter = adaptador
    }
}